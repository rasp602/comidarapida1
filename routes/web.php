<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*principal*/
Route::get('index','FromController@index');
Route::get('/','FromController@index');
Route::get('contacto','FromController@contacto');
Route::get('comidas','FromController@comidas');

/*admin*/
Route::get('admin','FromController@admin');
/*usuario*/
Route::resource('usuario', 'UsuarioController');
/*Login*/
Route::resource('log', 'LogController');
Route::get('logout', 'LogController@logout');

// /*productos*/
Route::resource('productos' ,'ProductosController');

/*Pruebas*/
Route::get('controlador', 'Controller@index');

