<?php

namespace comidarapida\Http\Controllers;

use Illuminate\Http\Request;
use comidarapida\Http\Controllers\Controller;
use comidarapida\Http\Requests;
use comidarapida\Productos;

class FromController extends Controller
{
    public function __construct(){
        $this->middleware('auth',['only'=>'admin']);
    }

    /**
     * 
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('index');
        return "estoy aqui";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
 

    public function comidas()
    {
        $productos = Productos::paginate(5);
        return view('comidas', compact('productos'));
    }

      public function admin()
    {
         return view('admin.index');
    }

    }
