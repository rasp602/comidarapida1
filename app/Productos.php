<?php

namespace comidarapida;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DB;
class Productos extends Model
{
        protected $table = "productos";
    	protected $fillable = ['name','path','descripcion','precio','duration'];

        public function setPathAttribute($path){
		$name = Carbon::now()->second.$path->getClientOriginalName();
		$this->attributes['path'] = $name;
		\Storage::disk('local')->put($name, \File::get($path));
	}

		public static function Productos(){
		return DB::table('productos')
		->select('productos.*')
		->get();
}

}
